<?php


namespace App\Controller;


use App\Entity\Feedback;
use App\Forms\FeedbackForm;
use App\Services\FeedbackService;
use App\Services\PhoneValidationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @var PhoneValidationService
     */
    private $phoneValidationService;
    /**
     * @var FeedbackService
     */
    private $feedbackService;

    public function __construct(PhoneValidationService $phoneValidationService, FeedbackService $feedbackService)
    {
        $this->phoneValidationService = $phoneValidationService;
        $this->feedbackService = $feedbackService;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(path="/index/index")
     */
    public function index(Request $request): Response
    {
        try {
            $feedback = new Feedback();
            $form = $this->createForm(FeedbackForm::class, $feedback);
            $form->handleRequest($request);

            $token = $request->request->get('feedback_form')['_csrf'];
            if ($request->isXmlHttpRequest() && $this->isCsrfTokenValid('feedback-form', $token))
            {
                $result = $this->phoneValidationService->isValid($feedback->getPhone());
                if ($result)
                {
                    $response = $this->phoneValidationService->getResponse();
                    $this->feedbackService->addFromExisting($feedback, $response);
                }

                return new JsonResponse(['result' => $result]);
            }

            return $this->render('index/index.html.twig',
                [
                    'form' => $form->createView()
                ]);
        } catch (\Throwable $exception) {
            return new Response($exception->getMessage());
        }
    }
}