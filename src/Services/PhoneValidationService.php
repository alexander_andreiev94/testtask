<?php


namespace App\Services;


use Symfony\Component\HttpFoundation\Response;

class PhoneValidationService
{
    private const API_URL = "http://apilayer.net/api/validate?";
    /**
     * @var string
     */
    private $accessKey;

    private $response;

    public function __construct(string $accessKey)
    {
        $this->accessKey = $accessKey;
    }

    public function isValid(string $phone): bool
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => self::API_URL.http_build_query([
                    'number' => $phone,
                    'access_key' => $this->accessKey
                ]),
            CURLOPT_RETURNTRANSFER => true
        ]);

        $response = curl_exec($ch);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code != Response::HTTP_OK)
            return false;

        $response = json_decode($response, true);
        $this->response = $response;

        return $response['valid'] ?? false;
    }

    public function getResponse(): array
    {
        return $this->response;
    }
}