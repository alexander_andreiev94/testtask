<?php


namespace App\Services;


use App\Entity\Feedback;
use Doctrine\ORM\EntityManagerInterface;

class FeedbackService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addFromExisting(Feedback $feedback, array $params): void
    {
        $feedback->setCarrier($params['carrier'] ?? null);
        $feedback->setCountry($params['country_code'] ?? null);

        $this->entityManager->persist($feedback);
        $this->entityManager->flush();
    }

    public function add(string $name, string $phone, string $message, array $params): Feedback
    {
        $feedback = new Feedback();
        $feedback->setPhone($phone);
        $feedback->setMessage($message);
        $feedback->setName($name);
        $feedback->setCarrier($params['carrier'] ?? null);
        $feedback->setCountry($params['country_code'] ?? null);

        $this->entityManager->persist($feedback);
        $this->entityManager->flush();

        return $feedback;
    }

    public function findOne(int $id): ?Feedback
    {
        return $this->entityManager->getRepository(Feedback::class)->find($id);
    }
}