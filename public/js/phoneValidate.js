$(document).ready(() => {
    $('#feedback-form').submit((e) => {
        e.preventDefault();

        const formData = $('#feedback-form').serialize();
        $.ajax({
            method: "POST",
            url: "/index/index",
            data: formData,
            success: successCb
        });
    });

    function successCb(response) {
        $("#msg-box-alert").removeClass("invisible");
        if (response.result) {
            $("#msg-box-alert").addClass("alert-success");
            $("#msg-box-alert").removeClass("alert-danger");
            $("#msg-box-alert").text("Thank you, we will contact you as soon as it possible !");
        } else {
            $("#msg-box-alert").addClass("alert-danger");
            $("#msg-box-alert").removeClass("alert-success");
            $("#msg-box-alert").text("Sorry, entered number is invalid");
        }
    }
});